# README #

Prerequisites:

Server de baze de date: MySQL (mysql-connector-java-5.1.40-bin.jar)
Libraria Primefaces (primefaces-6.0.jar)
MySQL Workbench
Netbeans 8.0.2
Glassfish 4.1


Instructiuni de descarcare a aplicatiei: 
	1. Conectare la Bitbucket 
	2. download 'Initial Commit' (codul sursa al proiectului cu toate resusele necesare (librarii, imagini))
	3. download 'Add database scripts' (script-urile necesare pentru crearea bazei de date)

Instructiuni de instalare si pornire a aplicatiei :
	1. Instalare  Server MySQL
	2. Pornire Server MySQL
	3. Instalare MySQL Workbench
	4. Conectare  la MySQL Workbench tool
	5. Executare scripturi in MySQL Workbench (copy-paste in fereastra de de SQL sau File -> Open SQL Scripts -> Pozitionare in directorul care contine scripturile -> Rulare pe rand)
	6. Instalare Netbeans 8.0.2 + Glassfish 4.1
	7. Pornire Netbeans 8.0.2 
	8. Pornire server
	9. Clonare proiect din Bitbucket
	10. Click dreapta din tab-ul Projects din Netbeans pe proiect -> Run
	